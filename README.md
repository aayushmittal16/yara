# Field management system

This service manages fields and provides weather history for a field with defined boundary data using a 3rd party API from OpenWeather Agro Monitoring.

## API Endpoints

| REST Endpoint               | Type                           | Description   |
|-----------------------------|--------------------------------|--------------|
| `/fields/{fieldId}`         | GET, PUT, DELETE               | Operations to manage fields data |
| `/fields`                   | POST                           | Create a new field by sending field data in the request body |
| `/fields/{fieldId}/weather` | GET                            | Retrieve weather data for field represented by `{fieldId}` |
| `/health`                   | GET                            | Check if the API is up and running

API specification can be seen on Swagger:

```
http://localhost:8080/swagger-ui.html
```

## Implementation

The general idea is that we want to use Open Weather Monitoring Agro API to manage our `Polygon` objects. But at the same time, it seems we represent these objects with the name `Boundaries`. My implementation basically registers every `Polygon` with the API and also creates a `Field` object with a `Boundaries` object wrapped inside. This means I am mapping our `Boundaries` with their `Polygon` objects and vice versa. Basically for `CREATE, UPDATE, DELETE` requests, I have to call this Open Agro API to register the state of a `Polygon` and then update our local database with the changes. For a `GET` request, we simply call the database.

## How to use this service

### Requirements
- Java 11+
- Docker

I am using docker-compose to containerize the API service and MongoDB individually so following is the command to get it up and running.
```
docker-compose up
```

For running unit and integration tests in an isolated container, following command works:
```
docker-compose -f docker-compose-test.yml up --exit-code-from tests
```

Access the API on your localhost:
```
POST http://localhost:8080/fields
```

## Scope for improvement

I ran out of time to spend on this task but following are the things I would consider before this API goes on production:

- `API Versioning` - It makes perfect sense to have this. The easiest way to start would be to have endpoint prefix such as `/api/v1/`. I have not introduced it here because I do not want to over-engineer this API for a prototype.
- `HATEOAS` - It would be an overkill to apply it for this service but generally it would be nice to navigate through a service without needing to have much prior knowledge of it.
- `Pagination` - This would make sense for a production grade service. Right now its not important because we are using CRUD on one field.
- `Validation` - It is not in scope of this task based on the contract.
- `Encryption` - would be a good idea to hide the API secret.
- `Logging` - Normally I use SLF4J for logging and log information on all levels. This is required because I want to have some details from the logs when an incident happens.
