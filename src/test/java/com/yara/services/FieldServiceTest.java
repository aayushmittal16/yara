package com.yara.services;

import static com.yara.utilities.AppUtils.getFileAsString;
import static com.yara.utilities.AppUtils.jsonStrToObjType;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import com.yara.dto.FieldDTO;
import com.yara.dto.PolygonDTO;
import com.yara.models.Field;
import com.yara.repositories.FieldRepository;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class FieldServiceTest {

  @InjectMocks
  private final FieldService fieldService = new FieldServiceImpl();
  @Mock
  private FieldRepository fieldRepository;
  @Mock
  private OpenAgroApiIntegrationService openAgroApi;

  private Field field;
  private String fieldId;

  @Before
  public void setUp() throws Exception {
    field = Field
        .convertToEntity(jsonStrToObjType(getFileAsString("field-fixture.json"), FieldDTO.class));
    fieldId = field.getId();
  }

  @Test
  public void When_getField_Then_Return_Field() {
    when(fieldRepository.findById(fieldId)).thenReturn(Optional.of(field));
    assertEquals(Optional.of(field), fieldService.getField(fieldId));
  }

  @Test
  public void When_deleteField_Then_Return_True() {
    when(openAgroApi.deletePolygon(field.getBoundaries().getId())).thenReturn(true);
    when(fieldService.getField(fieldId)).thenReturn(Optional.ofNullable(field));
    assertTrue(fieldService.deleteField(field.getId()));
  }

  @Test
  public void When_createField_Then_Return_Field() {
    PolygonDTO polygon = PolygonDTO.convertFromField(field.getName(), field.getBoundaries());
    when(openAgroApi.createPolygon(polygon)).thenReturn(Optional.of(field.getBoundaries()));
    assertEquals(Optional.of(field), fieldService.createField(field));
  }

  @Test
  public void When_updateField_Then_Return_UpdatedField() {
    PolygonDTO polygon = PolygonDTO.convertFromField(field.getName(), field.getBoundaries());
    when(fieldService.getField(fieldId)).thenReturn(Optional.ofNullable(field));
    when(openAgroApi.updatePolygon(polygon)).thenReturn(Optional.of(field.getBoundaries()));
    assertEquals(Optional.of(field), fieldService.updateField(field));
  }
}
