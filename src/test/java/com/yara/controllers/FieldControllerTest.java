package com.yara.controllers;

import static com.yara.utilities.AppUtils.getFileAsString;
import static com.yara.utilities.AppUtils.jsonStrToObjType;
import static com.yara.utilities.AppUtils.minifyJson;
import static com.yara.utilities.AppUtils.objToJsonStr;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.yara.dto.FieldDTO;
import com.yara.dto.WeatherWrapper;
import com.yara.models.Field;
import com.yara.models.Weather;
import com.yara.services.FieldService;
import java.util.Map;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@WebMvcTest(FieldController.class)
public class FieldControllerTest {

  String fieldStr;
  Field field;

  @Autowired
  private MockMvc mockMvc;
  @MockBean
  private FieldService fieldService;
  @Mock
  private RestTemplate restTemplate;

  @Before
  public void setUp() throws Exception {
    fieldStr = getFileAsString("field-fixture.json");
    field = Field
        .convertToEntity(jsonStrToObjType(getFileAsString("field-fixture.json"), FieldDTO.class));
  }

  @Test
  public void When_GetField_Then_Return_200_With_FieldObject() throws Exception {
    String id = "a0f63e74-d7ef-4924-acb3-0e770ae9ec98";
    when(fieldService.getField(id)).thenReturn(Optional.of(field));
    mockMvc.perform(get("/fields/" + id))
        .andExpect(status().isOk())
        .andExpect(content().string(minifyJson(fieldStr)));
  }

  @Test
  public void When_GetField_And_IdDoesNotExist_Then_Return_404() throws Exception {
    when(fieldService.getField(field.getId())).thenReturn(Optional.empty());
    mockMvc.perform(get("/fields/" + field.getId()))
        .andExpect(status().isNotFound());
  }

  @Test
  public void When_CreateField_Then_Return_201_Response_And_NewFieldObject() throws Exception {
    when(fieldService.createField(field)).thenReturn(Optional.of(field));
    mockMvc.perform(post("/fields")
        .content(fieldStr)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated())
        .andExpect(content().string(minifyJson(fieldStr)));
  }

  @Test
  public void When_CreateField_And_IdExists_Then_Return_400_Response()
      throws Exception {
    when(fieldService.createField(field)).thenReturn(Optional.empty());
    mockMvc.perform(post("/fields")
        .content(fieldStr)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());
  }

  @Test
  public void When_UpdateField_Then_Return_200_Response_And_UpdatedFieldObject() throws Exception {
    when(fieldService.updateField(field)).thenReturn(Optional.ofNullable(field));
    mockMvc.perform(put("/fields/" + field.getId())
        .content(fieldStr)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().string(minifyJson(fieldStr)));
  }

  @Test
  public void When_UpdateField_With_NonMatchingQueryParamAndFieldData_Then_Return_400_Response()
      throws Exception {
    when(fieldService.updateField(field)).thenReturn(Optional.ofNullable(field));
    mockMvc.perform(put("/fields/123")
        .content(fieldStr)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());
  }

  @Test
  public void When_UpdateField_With_NonExistentFieldId_Then_Return_404_Response() throws Exception {
    when(fieldService.updateField(field)).thenReturn(Optional.empty());
    mockMvc.perform(put("/fields/" + field.getId())
        .content(fieldStr)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
  }

  @Test
  public void When_DeleteField_Then_Return_204_Response() throws Exception {
    when(fieldService.deleteField(field.getId())).thenReturn(true);
    mockMvc.perform(delete("/fields/" + field.getId())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isNoContent());
  }

  @Test
  public void When_DeleteField_With_NonExistentId_Then_Return_404_Response() throws Exception {
    when(fieldService.deleteField(field.getId())).thenReturn(false);
    mockMvc.perform(delete("/fields/" + field.getId()))
        .andExpect(status().isNotFound());
  }

  @Test
  public void When_getWeatherHistoryForFieldBoundary_Then_Return_WeatherHistory()
      throws Exception {
    String fieldId = "a0f63e74-d7ef-4924-acb3-0e770ae9ec98";
    String weatherDataStr = getFileAsString("weather-fixture.json");
    WeatherWrapper weatherWrapper = jsonStrToObjType(weatherDataStr, WeatherWrapper.class);
    when(restTemplate.getForEntity(
        "https://samples.openweathermap.org/agro/1.0/weather/history?polyid={polyid}&appid={appid}&start={start}&end={end}",
        Weather[].class, mock(Map.class)))
        .thenReturn(new ResponseEntity<>(weatherWrapper.getWeatherData(), HttpStatus.OK));

    mockMvc.perform(get("/fields/a0f63e74-d7ef-4924-acb3-0e770ae9ec98/weather"))
        .andExpect(status().isOk())
        .andExpect(content().string(objToJsonStr(weatherWrapper)));
  }
}
