package com.yara.repositories;

import static com.yara.utilities.AppUtils.getFileAsString;
import static com.yara.utilities.AppUtils.jsonStrToObjType;
import static org.junit.Assert.assertTrue;

import com.yara.dto.FieldDTO;
import com.yara.models.Field;
import java.io.IOException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataMongoTest
public class FieldRepositoryTest {

  @Autowired
  private FieldRepository fieldRepository;

  private Field field;

  @Before
  public void setUp() throws IOException {
    field = Field
        .convertToEntity(jsonStrToObjType(getFileAsString("field-fixture.json"), FieldDTO.class));
  }

  @Test
  public void When_existsById_Then_True() {
    fieldRepository.save(field);

    assertTrue(fieldRepository.existsById(field.getId()));
  }
}
