package com.yara;

import static com.yara.utilities.AppUtils.getFileAsString;
import static com.yara.utilities.AppUtils.jsonStrToObjType;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import com.yara.dto.FieldDTO;
import com.yara.dto.PolygonDTO;
import com.yara.models.Field;
import com.yara.repositories.FieldRepository;
import com.yara.services.OpenAgroApiIntegrationService;
import java.util.Optional;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class FieldIntegrationTests {

  @LocalServerPort
  private int port;

  @MockBean
  private OpenAgroApiIntegrationService openAgroService;

  @Autowired
  private FieldRepository fieldRepository;

  @Autowired
  private TestRestTemplate restTemplate;

  private String baseUrl;
  private Field field;

  @Before
  public void setUp() throws Exception {
    baseUrl = "http://localhost:" + port;
    field = Field
        .convertToEntity(jsonStrToObjType(getFileAsString("field-fixture.json"), FieldDTO.class));
    fieldRepository.save(field);
  }

  @After
  public void tearDown() {
    fieldRepository.deleteAll();
  }

  @Test
  public void testGetField() {
    FieldDTO actualDTO = restTemplate
        .getForObject(baseUrl + "/fields/" + field.getId(), FieldDTO.class);
    FieldDTO expectedDTO = FieldDTO.convertFromEntity(field);
    assertEquals(expectedDTO, actualDTO);
  }

  @Test
  public void testCreateField() throws Exception {
    when(openAgroService
        .createPolygon(PolygonDTO.convertFromField(field.getName(), field.getBoundaries())))
        .thenReturn(Optional.ofNullable(field.getBoundaries()));
    ResponseEntity<FieldDTO> responseEntity = restTemplate
        .postForEntity(baseUrl + "/fields", FieldDTO.convertFromEntity(field), FieldDTO.class);
    assertEquals(201, responseEntity.getStatusCodeValue());
    assertNotNull(responseEntity.getBody());
    assertEquals(FieldDTO.convertFromEntity(field), responseEntity.getBody());
  }

  @Test
  public void testUpdateField() throws Exception {
    when(openAgroService
        .updatePolygon(PolygonDTO.convertFromField(field.getName(), field.getBoundaries())))
        .thenReturn(Optional.ofNullable(field.getBoundaries()));
    ResponseEntity<FieldDTO> responseEntity = restTemplate
        .exchange(baseUrl + "/fields/" + field.getId(), HttpMethod.PUT,
            new HttpEntity<>(FieldDTO.convertFromEntity(field)),
            FieldDTO.class);
    assertEquals(200, responseEntity.getStatusCodeValue());
    assertNotNull(responseEntity.getBody());
    assertEquals(FieldDTO.convertFromEntity(field), responseEntity.getBody());
  }

  @Test
  public void testDeleteField() {
    when(openAgroService.deletePolygon(field.getBoundaries().getId())).thenReturn(true);
    restTemplate.delete(baseUrl + "/fields/" + field.getId());
    assertFalse(fieldRepository.existsById(field.getId()));
  }
}
