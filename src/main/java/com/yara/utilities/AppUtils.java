package com.yara.utilities;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.nio.file.Files;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import org.springframework.core.io.ClassPathResource;

public class AppUtils {

  private static final ObjectMapper objectMapper = new ObjectMapper();

  /**
   * Convert pretty JSON into minified JSON
   *
   * @param prettyJson pretty JSON
   * @return minified JSON
   * @throws JsonProcessingException
   */
  public static String minifyJson(String prettyJson) throws JsonProcessingException {
    return objectMapper.readValue(prettyJson, JsonNode.class).toString();
  }

  /**
   * Convert an object into a JSON string
   *
   * @param object object to be converted
   * @return a JSON string
   * @throws JsonProcessingException
   */
  public static String objToJsonStr(Object object) throws JsonProcessingException {
    return objectMapper.writeValueAsString(object);
  }

  /**
   * Convert a JSON string to an internal model object
   *
   * @param json json to convert
   * @param type type of Class to convert to
   * @param <T>  generic type
   * @return an object of type T populated with data from json
   * @throws JsonProcessingException
   */
  public static <T> T jsonStrToObjType(String json, Class<T> type) throws JsonProcessingException {
    return objectMapper.readValue(json, type);
  }

  /**
   * Fetch a classpath resource from the given path and read its content into a string
   *
   * @param path location of the file on classpath
   * @return string representing the content of the file
   * @throws IOException
   */
  public static String getFileAsString(String path) throws IOException {
    return Files.readString(new ClassPathResource(path).getFile().toPath());
  }

  /**
   * Returns the immediate instant with millisecond precision in ISO-8601 format. This specific
   * format is used in Field and Boundary models.
   *
   * @return Instant as a string
   */
  public static String now() {
    return unixTimeToISO8601(Instant.now().getEpochSecond());
  }

  /**
   * Convert unix time to ISO 8601 format.
   *
   * @param unixTime long representing unix time.
   * @return string representation of ISO 8601 date
   */
  public static String unixTimeToISO8601(long unixTime) {
    return Instant.ofEpochMilli(unixTime * 1000).toString();
  }

  /**
   * Convert ISO 8601 time to unix time long
   * @param time
   * @return long value
   */
  public static long iso8601ToUnixTime(String time) {
    return Instant.parse(time).toEpochMilli();
  }

  /**
   * Returns time daysOffset number of days from now in UNIX time format
   *
   * @param daysOffset number of days before or after now
   * @return long representing the UNIX time
   */
  public static long unixTimeDaysAgo(int daysOffset) {
    return ZonedDateTime.now(ZoneOffset.UTC).minusDays(daysOffset).toInstant().getEpochSecond();
  }
}
