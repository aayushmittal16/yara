package com.yara.services;

import com.yara.dto.PolygonDTO;
import com.yara.models.Boundaries;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class OpenAgroApiIntegrationService {

  private static final String QUERY_PARAMS_FORMAT = "?appid={appid}";
  private final RestTemplate restTemplate = new RestTemplate();

  @Value("${agro-monitoring-polygon-api.app-id}")
  private String polygonAppId;
  @Value("${agro-monitoring-polygon-api.base-url}")
  private String polygonEndpoint;

  public Optional<Boundaries> getPolygon(String id) {
    ResponseEntity<PolygonDTO> responseEntity = restTemplate
        .getForEntity(polygonEndpoint + "/{id}" + QUERY_PARAMS_FORMAT, PolygonDTO.class,
            Map.of("appid", polygonAppId, "id", id));
    return Optional.of(Boundaries.convertToEntity(responseEntity.getBody()));
  }

  public Optional<Boundaries> createPolygon(PolygonDTO polygon) {
    ResponseEntity<PolygonDTO> responseEntity = restTemplate
        .postForEntity(polygonEndpoint + QUERY_PARAMS_FORMAT, polygon, PolygonDTO.class,
            Map.of("appid", polygonAppId));
    return Optional.of(Boundaries.convertToEntity(responseEntity.getBody()));
  }

  public Optional<Boundaries> updatePolygon(PolygonDTO polygon) {
    ResponseEntity<PolygonDTO> responseEntity = restTemplate
        .exchange(polygonEndpoint + "/{id}" + QUERY_PARAMS_FORMAT, HttpMethod.PUT,
            new HttpEntity<>(polygon), PolygonDTO.class,
            Map.of("appid", polygonAppId, "id", polygon.getId()));
    return Optional.of(Boundaries.convertToEntity(responseEntity.getBody()));
  }

  public boolean deletePolygon(String id) {
    ResponseEntity<Void> responseEntity = restTemplate
        .exchange(polygonEndpoint + "/{id}" + QUERY_PARAMS_FORMAT, HttpMethod.DELETE,
            new HttpEntity<>(null), Void.class, Map.of("appid", polygonAppId, "id", id));
    return responseEntity.getStatusCodeValue() == 204;
  }
}
