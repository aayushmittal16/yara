package com.yara.services;

import static com.yara.utilities.AppUtils.now;

import com.yara.dto.PolygonDTO;
import com.yara.models.Boundaries;
import com.yara.models.Field;
import com.yara.repositories.FieldRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FieldServiceImpl implements FieldService {

  @Autowired
  private FieldRepository fieldRepository;

  @Autowired
  private OpenAgroApiIntegrationService openAgroApiService;

  @Override
  public Optional<Field> getField(String fieldId) {
    return fieldRepository.findById(fieldId);
  }

  @Override
  public Optional<Field> createField(Field field) {
    Boundaries newBoundaries = openAgroApiService
        .createPolygon(PolygonDTO.convertFromField(field.getName(), field.getBoundaries()))
        .orElse(null);
    if (newBoundaries == null) {
      return Optional.empty();
    }
    field.setBoundaries(newBoundaries);
    field.setCreated(newBoundaries.getCreated());
    fieldRepository.save(field);
    return Optional.of(field);
  }

  @Override
  public Optional<Field> updateField(Field field) {
    Optional<Field> fieldFromDB = getField(field.getId());
    if (fieldFromDB.isEmpty()) {
      return Optional.empty();
    }
    Boundaries updatedBoundaries = openAgroApiService
        .updatePolygon(PolygonDTO.convertFromField(field.getName(), field.getBoundaries()))
        .orElse(null);
    if (updatedBoundaries == null) {
      return Optional.empty();
    }
    updatedBoundaries.setUpdated(now());
    field.setUpdated(updatedBoundaries.getUpdated());

    // Retain creation time from the database
    field.setCreated(fieldFromDB.get().getCreated());
    field.setBoundaries(updatedBoundaries);

    fieldRepository.save(field);
    return Optional.of(field);
  }

  @Override
  public boolean deleteField(String fieldId) {
    Optional<Field> field = getField(fieldId);
    if (field.isPresent() && openAgroApiService
        .deletePolygon(field.get().getBoundaries().getId())) {
      fieldRepository.deleteById(fieldId);
      return true;
    }
    return false;
  }
}
