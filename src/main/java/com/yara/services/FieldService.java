package com.yara.services;

import com.yara.models.Field;
import java.util.Optional;

public interface FieldService {

  Optional<Field> getField(String fieldId);

  Optional<Field> createField(Field field);

  Optional<Field> updateField(Field field);

  boolean deleteField(String fieldId);
}
