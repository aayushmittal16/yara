package com.yara.controllers;

import com.yara.dto.FieldDTO;
import com.yara.dto.WeatherWrapper;
import com.yara.models.Field;
import com.yara.models.Weather;
import com.yara.services.FieldService;
import com.yara.utilities.AppUtils;
import java.net.URI;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/fields")
public class FieldController {

  private final RestTemplate restTemplate = new RestTemplate();
  @Autowired
  FieldService fieldService;

  @Value("${agro-monitoring-weather-history-api.app-id}")
  private String weatherHistoryAppId;
  @Value("${agro-monitoring-weather-history-api.base-url}")
  private String weatherHistoryEndpoint;

  @GetMapping("/{fieldId}")
  public ResponseEntity<FieldDTO> getField(
      @PathVariable("fieldId") @NotEmpty String fieldId) {
    return fieldService.getField(fieldId).map(FieldDTO::convertFromEntity).map(ResponseEntity::ok)
        .orElseGet(() -> ResponseEntity.notFound().build());
  }

  @PostMapping
  public ResponseEntity<Object> createField(@RequestBody() @NotNull FieldDTO fieldDTO) {
    Optional<Field> createdField = fieldService.createField(Field.convertToEntity(fieldDTO));
    if (createdField.isEmpty()) {
      return ResponseEntity.badRequest().build();
    }
    String path = String.format("/fields/%s", createdField.get().getId());
    return ResponseEntity.created(URI.create(path))
        .body(createdField.map(FieldDTO::convertFromEntity));
  }

  @PutMapping("/{fieldId}")
  public ResponseEntity<FieldDTO> updateField(@PathVariable("fieldId") @NotEmpty String fieldId,
      @RequestBody() @NotNull FieldDTO fieldDTO) {
    if (!fieldId.equals(fieldDTO.getId())) {
      return ResponseEntity.badRequest().build();
    }
    Optional<Field> updatedField = fieldService.updateField(Field.convertToEntity(fieldDTO));
    return updatedField.map(FieldDTO::convertFromEntity).map(ResponseEntity::ok)
        .orElseGet(() -> ResponseEntity.notFound().build());
  }

  @DeleteMapping("/{fieldId}")
  public ResponseEntity<Object> deleteField(@PathVariable("fieldId") @NotEmpty String fieldId) {
    if (!fieldService.deleteField(fieldId)) {
      return ResponseEntity.notFound().build();
    }
    return ResponseEntity.noContent().build();
  }

  @GetMapping("/{fieldId}/weather")
  public ResponseEntity<WeatherWrapper> getWeatherHistoryForFieldBoundary(
      @PathVariable("fieldId") @NotEmpty String fieldId) {
    String endpoint =
        weatherHistoryEndpoint + "?polyid={polyid}&appid={appid}&start={start}&end={end}";
    ResponseEntity<Weather[]> responseEntity = restTemplate.getForEntity(endpoint,
        Weather[].class, queryParamsForWeatherHistoryApi(fieldId));
    return new ResponseEntity<>(new WeatherWrapper(responseEntity.getBody()),
        responseEntity.getStatusCode());
  }

  private Map<String, Object> queryParamsForWeatherHistoryApi(String polyId) {
    Map<String, Object> queryParams = new LinkedHashMap<>();
    queryParams.put("polyid", polyId);
    queryParams.put("appid", weatherHistoryAppId);
    queryParams.put("start", AppUtils.unixTimeDaysAgo(7));
    queryParams.put("end", AppUtils.unixTimeDaysAgo(0));
    return queryParams;
  }
}
