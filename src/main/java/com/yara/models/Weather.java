package com.yara.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.Map;

@JsonPropertyOrder({"timestamp", "temperature", "humidity", "temperatureMin", "temperatureMax"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Weather {

  private String timestamp;
  private double temperature;
  private int humidity;
  private double temperatureMin;
  private double temperatureMax;

  public Weather() {
  }

  @JsonProperty("main")
  public void unMarshaller(Map<String, Number> objects) {
    temperature = (double) objects.getOrDefault("temp", 0.0);
    humidity = (int) objects.getOrDefault("humidity", 0.0);
    temperatureMin = (double) objects.getOrDefault("temp_min", 0.0);
    temperatureMax = (double) objects.getOrDefault("temp_max", 0.0);
  }

  public String getTimestamp() {
    return timestamp;
  }

  @JsonProperty("dt")
  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  public double getTemperature() {
    return temperature;
  }

  public void setTemperature(double temperature) {
    this.temperature = temperature;
  }

  public int getHumidity() {
    return humidity;
  }

  public void setHumidity(int humidity) {
    this.humidity = humidity;
  }

  public double getTemperatureMin() {
    return temperatureMin;
  }

  public void setTemperatureMin(double temperatureMin) {
    this.temperatureMin = temperatureMin;
  }

  public double getTemperatureMax() {
    return temperatureMax;
  }

  public void setTemperatureMax(double temperatureMax) {
    this.temperatureMax = temperatureMax;
  }
}
