package com.yara.models;

import com.yara.dto.FieldDTO;
import java.util.Objects;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "fields")
public class Field {

  @Id
  private String id;
  private String name;
  private String created;
  private String updated;
  private String countryCode;
  private Boundaries boundaries;

  public Field() {
  }

  public Field(String id, String name, String created, String updated, String countryCode,
      Boundaries boundaries) {
    this.id = id;
    this.name = name;
    this.created = created;
    this.updated = updated;
    this.countryCode = countryCode;
    this.boundaries = boundaries;
  }

  public static Field convertToEntity(FieldDTO dto) {
    if (dto == null) {
      return new Field();
    }
    return new Field(dto.getId(), dto.getName(), dto.getCreated(), dto.getUpdated(),
        dto.getCountryCode(), Boundaries.convertToEntity(dto.getBoundariesDTO()));
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCreated() {
    return created;
  }

  public void setCreated(String created) {
    this.created = created;
  }

  public String getUpdated() {
    return updated;
  }

  public void setUpdated(String updated) {
    this.updated = updated;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public Boundaries getBoundaries() {
    return boundaries;
  }

  public void setBoundaries(Boundaries boundaries) {
    this.boundaries = boundaries;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Field that = (Field) o;
    return id.equals(that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
