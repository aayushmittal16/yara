package com.yara.models;

import static com.yara.utilities.AppUtils.unixTimeToISO8601;

import com.yara.dto.BoundariesDTO;
import com.yara.dto.PolygonDTO;
import org.springframework.data.annotation.Id;

public class Boundaries {

  @Id
  private String id;
  private String created;
  private String updated;
  private GeoJson geoJson;

  public Boundaries() {
  }

  public Boundaries(String id, String created, String updated, GeoJson geoJson) {
    this.id = id;
    this.created = created;
    this.updated = updated;
    this.geoJson = geoJson;
  }

  public static Boundaries convertToEntity(BoundariesDTO dto) {
    if (dto == null) {
      return new Boundaries();
    }
    return new Boundaries(dto.getId(), dto.getCreated(), dto.getUpdated(),
        GeoJson.convertToEntity(dto.getGeoJsonDTO()));
  }

  public static Boundaries convertToEntity(PolygonDTO dto) {
    if (dto == null) {
      return new Boundaries();
    }
    return new Boundaries(dto.getId(), unixTimeToISO8601(dto.getCreated()), null,
        GeoJson.convertToEntity(dto.getGeoJsonDTO()));
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getCreated() {
    return created;
  }

  public void setCreated(String created) {
    this.created = created;
  }

  public String getUpdated() {
    return updated;
  }

  public void setUpdated(String updated) {
    this.updated = updated;
  }

  public GeoJson getGeoJson() {
    return geoJson;
  }

  public void setGeoJson(GeoJson geoJson) {
    this.geoJson = geoJson;
  }
}
