package com.yara.models;

import com.yara.dto.GeoJsonDTO;
import com.yara.dto.GeoJsonDTO.GeometryDTO;
import java.util.Properties;

public class GeoJson {

  private String type;
  private Properties properties;
  private Geometry geometry;

  public GeoJson() {
  }

  public GeoJson(String type, Properties properties, Geometry geometry) {
    this.type = type;
    this.properties = properties;
    this.geometry = geometry;
  }

  public static GeoJson convertToEntity(GeoJsonDTO dto) {
    if (dto == null) {
      return new GeoJson();
    }
    return new GeoJson(dto.getType(), dto.getProperties(),
        Geometry.convertToEntity(dto.getGeometryDTO()));
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Properties getProperties() {
    return properties;
  }

  public void setProperties(Properties properties) {
    this.properties = properties;
  }

  public Geometry getGeometry() {
    return geometry;
  }

  public void setGeometry(Geometry geometry) {
    this.geometry = geometry;
  }

  public static class Geometry {

    String type;
    double[][][] coordinates;

    public Geometry() {
    }

    public Geometry(String type, double[][][] coordinates) {
      this.type = type;
      this.coordinates = coordinates;
    }

    public static Geometry convertToEntity(GeometryDTO dto) {
      if (dto == null) {
        return new Geometry();
      }
      return new Geometry(dto.getType(), dto.getCoordinates());
    }

    public String getType() {
      return type;
    }

    public void setType(String type) {
      this.type = type;
    }

    public double[][][] getCoordinates() {
      return coordinates;
    }

    public void setCoordinates(double[][][] coordinates) {
      this.coordinates = coordinates;
    }
  }
}