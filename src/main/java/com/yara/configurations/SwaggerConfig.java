package com.yara.configurations;

import com.yara.dto.BoundariesDTO;
import com.yara.dto.FieldDTO;
import com.yara.dto.GeoJsonDTO;
import com.yara.dto.GeoJsonDTO.GeometryDTO;
import com.yara.dto.WeatherWrapper;
import com.yara.models.Weather;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

  private final Class[] clazz = {BoundariesDTO.class, FieldDTO.class, GeoJsonDTO.class, GeometryDTO.class,
      ModelAndView.class, View.class, Weather.class, WeatherWrapper.class};

  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
        .select()
        .apis(RequestHandlerSelectors.any())
        .paths(PathSelectors.ant("/fields/**"))
        .build()
        .ignoredParameterTypes(clazz);
  }
}