package com.yara.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.yara.models.Field;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FieldDTO {

  private String id;
  private String name;
  private String created;
  private String updated;
  private String countryCode;
  @JsonProperty("boundaries")
  private BoundariesDTO boundariesDTO;

  public FieldDTO() {
  }

  public FieldDTO(String id, String name, String created, String updated, String countryCode,
      BoundariesDTO boundariesDTO) {
    this.id = id;
    this.name = name;
    this.countryCode = countryCode;
    this.boundariesDTO = boundariesDTO;
    this.updated = updated;
    this.created = created;
  }

  public static FieldDTO convertFromEntity(Field field) {
    if (field == null) {
      return new FieldDTO();
    }
    return new FieldDTO(field.getId(), field.getName(), field.getCreated(),
        field.getUpdated(), field.getCountryCode(),
        BoundariesDTO.convertFromBoundaries(field.getBoundaries()));
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCreated() {
    return created;
  }

  public void setCreated(String created) {
    this.created = created;
  }

  public String getUpdated() {
    return updated;
  }

  public void setUpdated(String updated) {
    this.updated = updated;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public BoundariesDTO getBoundariesDTO() {
    return boundariesDTO;
  }

  public void setBoundariesDTO(BoundariesDTO boundariesDTO) {
    this.boundariesDTO = boundariesDTO;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    FieldDTO field = (FieldDTO) o;
    return id.equals(field.id) &&
        Objects.equals(boundariesDTO, field.boundariesDTO);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, boundariesDTO);
  }
}
