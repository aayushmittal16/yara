package com.yara.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.yara.models.Weather;

public class WeatherWrapper {

  @JsonProperty("weather")
  Weather[] weatherData;

  public WeatherWrapper() {
  }

  public WeatherWrapper(Weather[] weatherData) {
    this.weatherData = weatherData;
  }

  public Weather[] getWeatherData() {
    return weatherData;
  }

  public void setWeatherData(Weather[] weatherData) {
    this.weatherData = weatherData;
  }
}
