package com.yara.dto;

import static com.yara.utilities.AppUtils.iso8601ToUnixTime;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.yara.models.Boundaries;
import java.util.Objects;
import org.springframework.data.annotation.Id;

/**
 * This class is almost identical to Boundaries but it represents an external data model. It has
 * additional fields of which `name` is a field required to call the API. The external API
 * recognizes GeoJson with key `geo_json`.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PolygonDTO {

  @Id
  private String id;
  private String name;
  @JsonProperty("geo_json")
  private GeoJsonDTO geoJsonDTO;
  @JsonProperty("created_at")
  private Long created;

  public PolygonDTO() {
  }

  public PolygonDTO(String id, String name,
      GeoJsonDTO geoJsonDTO, Long created) {
    this.id = id;
    this.name = name;
    this.geoJsonDTO = geoJsonDTO;
    this.created = created;
  }

  public static PolygonDTO convertFromField(String name, Boundaries boundaries) {
    if (boundaries == null) {
      return new PolygonDTO();
    }
    Long created =
        boundaries.getCreated() == null || boundaries.getCreated().trim().length() == 0 ? null
            : iso8601ToUnixTime(boundaries.getCreated());
    return new PolygonDTO(boundaries.getId(), name,
        GeoJsonDTO.convertFromEntity(boundaries.getGeoJson()),
        created);
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public GeoJsonDTO getGeoJsonDTO() {
    return geoJsonDTO;
  }

  public void setGeoJsonDTO(GeoJsonDTO geoJsonDTO) {
    this.geoJsonDTO = geoJsonDTO;
  }

  public Long getCreated() {
    return created;
  }

  public void setCreated(Long created) {
    this.created = created;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PolygonDTO polygon = (PolygonDTO) o;
    return id.equals(polygon.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
