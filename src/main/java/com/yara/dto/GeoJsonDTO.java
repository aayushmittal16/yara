package com.yara.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.yara.models.GeoJson;
import com.yara.models.GeoJson.Geometry;
import java.util.Properties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GeoJsonDTO {

  private String type;
  private Properties properties;
  @JsonProperty("geometry")
  private GeometryDTO geometryDTO;

  public GeoJsonDTO() {
  }

  public GeoJsonDTO(String type, Properties properties,
      GeometryDTO geometryDTO) {
    this.type = type;
    this.properties = properties;
    this.geometryDTO = geometryDTO;
  }

  public static GeoJsonDTO convertFromEntity(GeoJson geoJson) {
    if (geoJson == null) {
      return new GeoJsonDTO();
    }
    return new GeoJsonDTO(geoJson.getType(), geoJson.getProperties(),
        GeometryDTO.convertFromEntity(
            geoJson.getGeometry()));
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Properties getProperties() {
    return properties;
  }

  public void setProperties(Properties properties) {
    this.properties = properties;
  }

  public GeometryDTO getGeometryDTO() {
    return geometryDTO;
  }

  public void setGeometryDTO(GeometryDTO geometryDTO) {
    this.geometryDTO = geometryDTO;
  }

  public static class GeometryDTO {

    String type;
    double[][][] coordinates;

    public GeometryDTO() {
    }

    public GeometryDTO(String type, double[][][] coordinates) {
      this.type = type;
      this.coordinates = coordinates;
    }

    public static GeometryDTO convertFromEntity(Geometry geometry) {
      if (geometry == null) {
        return new GeometryDTO();
      }
      return new GeometryDTO(geometry.getType(), geometry.getCoordinates());
    }

    public String getType() {
      return type;
    }

    public void setType(String type) {
      this.type = type;
    }

    public double[][][] getCoordinates() {
      return coordinates;
    }

    public void setCoordinates(double[][][] coordinates) {
      this.coordinates = coordinates;
    }
  }
}