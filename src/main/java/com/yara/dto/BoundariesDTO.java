package com.yara.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.yara.models.Boundaries;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BoundariesDTO {

  private String id;
  private String created;
  private String updated;
  @JsonProperty("geoJson")
  private GeoJsonDTO geoJsonDTO;

  public BoundariesDTO() {
  }

  public BoundariesDTO(String id, String created, String updated, GeoJsonDTO geoJsonDTO) {
    this.id = id;
    this.geoJsonDTO = geoJsonDTO;
    this.created = created;
    this.updated = updated;
  }

  public static BoundariesDTO convertFromBoundaries(Boundaries boundaries) {
    if (boundaries == null) {
      return new BoundariesDTO();
    }
    return new BoundariesDTO(boundaries.getId(), boundaries.getCreated(), boundaries.getUpdated(),
        GeoJsonDTO.convertFromEntity(boundaries.getGeoJson()));
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getCreated() {
    return created;
  }

  public void setCreated(String created) {
    this.created = created;
  }

  public String getUpdated() {
    return updated;
  }

  public void setUpdated(String updated) {
    this.updated = updated;
  }

  public GeoJsonDTO getGeoJsonDTO() {
    return geoJsonDTO;
  }

  public void setGeoJsonDTO(GeoJsonDTO geoJsonDTO) {
    this.geoJsonDTO = geoJsonDTO;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BoundariesDTO that = (BoundariesDTO) o;
    return id.equals(that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
